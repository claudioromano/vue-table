Vue.use(VueTables.server, {
  compileTemplates: true,
  //highlightMatches: true,
  //pagination: {
  // dropdown:true
  // chunk:5
  // },
  filterByColumn: true,
  texts: {
    filter: "Search:"
  },
  datepickerOptions: {
    showDropdowns: true
  }
  //skin:''
});
new Vue({
  el: "#app",
  data: {
    search: '',
    columns: ['id','nome','pedido','status','pagamento','periodo'],
     customFilters: ['nome'],
     customFilters: [
        {
          name:'nome',
          callback: function(row, query) {
            return row.name[0] == query;
      
        }
        }
      ],
    options: {
      dateColumns: ['periodo'],
        filterByColumn: true,
        filterable: ['id','nome','pedido','status','pagamento','periodo'],
        listColumns: {
          status: [{
            id: 0,
            text: 'pendente'
          }, {
            id: 1,
            text: 'finalizado'
          }],
          pagamento: [{
            id: 0,
            text: 'boleto'
          }, {
            id: 1,
            text: 'parcelado'
          }]
        },
    },
    customFilters: [
      {
          name:'name',
          callback: function(row, query) {
          return row.name[0] == query;
        }
      }
    ]
  }
});
