import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import VueResource from "vue-resource"
import App from './App.vue'
import Jquery from 'jquery'
import Moment from 'moment'
import VueTable from 'vue-tables-2'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

import {ServerTable, ClientTable, Event} from 'vue-tables-2'


Vue.use(Jquery)
Vue.use(Moment)
Vue.use(BootstrapVue)
Vue.use(VueTable)
 Vue.use(ServerTable)
 Vue.use(VueResource)

new Vue({
  el: '#app',
  render: h => h(App)
})


Vue.use(ServerTable, {
  compileTemplates: true,
  //highlightMatches: true,
  //pagination: {
  // dropdown:true
  // chunk:5
  // },
  filterByColumn: true,
  texts: {
    filter: "Search:"
  },
  datepickerOptions: {
    showDropdowns: true
  }
});
new Vue({
  el: "#people",
  data: {
    columns: ['id','nome','pedido','status','pagamento','periodo'],
    options: {
      	dateColumns: ['periodo'],
        filterByColumn: true,
        filterable: ['id','nome','pedido','status','pagamento','periodo'],
        listColumns: {
          status: [{
            id: 0,
            text: 'pendente'
          }, {
            id: 1,
            text: 'finalizado'
          }],
          pagamento: [{
            id: 0,
            text: 'boleto'
          }, {
            id: 1,
            text: 'parcelado'
          }]
        },
    },
    customFilters: [
      {
          name:'name',
          callback: function(row, query) {
          return row.name[0] == query;
        }
      }
    ]
  }
});

